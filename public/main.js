const searchInput = document.getElementById('search-input')
const qrInput = document.getElementById('qr-input')
const apiBase = 'http://114.119.180.190:10010'
// const apiBase = 'http://192.168.2.57:9110'

function getJWT() {
    return sessionStorage.getItem('jwt')
}

function getTrackID() {
    return sessionStorage.getItem('trackID')
}

function prepareSearch() {
    if (!searchInput)
        return
    const ul = document.getElementById('user-list')
    let lastSearch = ''
    searchInput.addEventListener('keyup', function (e) {
        if (searchInput.value.length < 2) {
            ul.innerHTML = ''
            return
        }
        if (lastSearch === searchInput.value)
            return
        lastSearch = searchInput.value
        fetch(apiBase + '/check-in/' + searchInput.value, {
            method: 'GET',
            headers: {
                'Authorization': getJWT(),
            },
        }).then(r => r.json()).then((body) => {
            ul.innerHTML = ''
            if (!body && !body.data)
                return
            body.data.slice(0, 3).map(u => {
                const li = document.createElement('li')
                li.innerText = `${u.first_name} ${u.last_name} - ${u.company_name}`
                li.tabIndex = 0
                li.onclick = () => {
                    confirmAttendee(u)
                    searchInput.value = ''
                    lastSearch = ''
                    ul.innerHTML = ''
                }
                ul.appendChild(li)
            })
        }).catch(() => {
            alert('Search failed. Please approach our staff.')
        })
    })
}

function prepareQR() {
    const form = document.getElementById('qr-form')
    if (!form)
        return
    function refocus() {
        setTimeout(() => {
            if (document.activeElement !== searchInput) {
                qrInput.focus()
            }
        })
    }
    form.addEventListener('submit', function (e) {
        e.preventDefault()
        if (!qrInput.value.length)
            return
        fetch(apiBase + '/check-in/uuid/' + qrInput.value, {
            method: 'GET',
            headers: {
                'Authorization': getJWT(),
            },
        }).then(r => r.json()).then((body) => {
            if (body.status == 200) {
                confirmAttendee(body.data)
            } else {
                alert('Check-in failed. Please approach our staff.')
            }
        }).catch(() => {
            alert('Check-in failed. Please approach our staff.')
        })
        qrInput.value = ''
    })
    qrInput.addEventListener('blur', refocus)
    document.body.addEventListener('click', refocus)
    setInterval(refocus, 500) // somehow alwys lose focus so just resort to this
    refocus()
}

function confirmAttendee(u) {
    const c = document.getElementById('confirmation')
    const [p1, p2] = c.querySelectorAll('p')
    p1.innerText = `Name: ${u.first_name} ${u.last_name}`
    p2.innerText = `Company: ${u.company_name}`
    c.style.display = 'block'
    c.querySelector('.confirm-btn').onclick = (e) => {
        e.preventDefault()
        checkIn(u.uuid)
        c.style.display = 'none'
    }
    c.querySelector('.cancel-btn').onclick = (e) => {
        e.preventDefault()
        c.style.display = 'none'
    }
}

function checkIn(uuid) {
    fetch(apiBase + '/check-in', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': getJWT(),
        },
        body: JSON.stringify({ uuid })
    }).then(r => r.json()).then((body) => {
        if (body.status == 200) {
            printCard(body.data)
            // alert('Check-in successful. Printing your card now.')
        } else {
            alert('Check-in failed. Please approach our staff.')
        }
    }).catch(() => {
        alert('Check-in failed. Please approach our staff.')
    })
    qrInput.focus()
}

function printCard(user) {
    fetch('/print', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            name: user.first_name + ' ' + user.last_name,
            company: user.company_name,
            url: user.url,
        })
    }).then((resp) => {
        if (resp.ok) {
            console.log('Card printed')
        } else {
            alert('Failed to print your card. Please approach our staff.')
        }
    }).catch(() => {
        alert('Failed to print your card. Please approach our staff.')
    })
}

function prepareAuth() {
    const form = document.getElementById('login-form')
    if (!form)
        return
    form.addEventListener('submit', function (e) {
        e.preventDefault()
        fetch(apiBase + '/check-in/authenticate', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ username: form.username.value, password: form.password.value })
        }).then((resp) => resp.json()).then((body) => {
            if (body.data.token) {
                sessionStorage.setItem('jwt', body.data.token)
                sessionStorage.setItem('trackID', body.data.track_id)
                location.href = "/"
            } else {
                alert('Login failed')
            }
        }).catch(() => {
            alert('Login failed')
        })
    })
}

function prepareRegister() {
    const form = document.getElementById('register-form')
    if (!form)
        return
    form.addEventListener('submit', function (e) {
        e.preventDefault();
        const ogText = form.button.innerText
        form.button.innerText = 'Loading...'
        form.button.disabled = true
        fetch(apiBase + '/check-in/register', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': getJWT(),
            },
            body: JSON.stringify({
                email: form.email.value,
                first_name: form.first_name.value,
                last_name: form.last_name.value,
                company_name: form.company_name.value,
            })
        }).then(r => r.json()).then(body => {
            if (body.status == 200) {
                form.email.value = ''
                form.first_name.value = ''
                form.last_name.value = ''
                form.company_name.value = ''
                printCard(body.data)
                // alert('Registration successful. Printing your card now.')
            } else {
                alert('Registration failed: Please check ensure all fields are correct.')
            }
        }).catch(() => {
            alert('Registration failed. Please approach our staff.')
        }).finally(() => {
            form.button.innerText = ogText
            form.button.disabled = false
        })

    })
}

if (!getJWT() && location.pathname != "/login.html") {
    location.href = "/login.html"
}

prepareAuth()
prepareRegister()
prepareSearch()
prepareQR()
