const express = require('express')
const app = express()
const port = 3000
const bodyParser = require('body-parser')
const { spawn } = require('child_process')

app.use(express.static('public'))
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const bin = 'printer/print.exe'
// const bin = 'C:/Users/dalton/Desktop/solid-310/bin/x64/Debug/netcoreapp3.1/print.exe'

const statusMap = {
    0: 'Ok',
    1: 'Unknown error',
    2: 'No printer detected',
    3: 'Connection failed',
    4: 'Card not going in',
    5: 'Card not going out',
    6: 'Card not moving',
    7: 'Draw error',
    8: 'Print error',
    9: 'Printer disconnected',
}

async function printCard(user) {
    return new Promise((resolve) => {
        const process = spawn(bin, ['register', user.name, user.company, user.url, "back"]); // remove "back" to send the card to the front
        // const process = spawn('C:/Program Files/Git/cmd/git.exe', ['--help']);
        process.on('exit', (code) => {
            console.log(`Printer exited with code ${code} - ${statusMap[code]}`);
            resolve(code);
        });
        process.on('error', (err) => {
            console.error('Printer failed: ' + err)
            reject(1)
        })
    })
}

async function wait(t) {
    return new Promise(resolve => {
        setTimeout(resolve, t)
    })
}

app.post('/print', async function (req, res) {
    const user = { ...req.body }
    let success = false
    for (let i = 0; i < 3; i++) {
        r = await printCard(user)
        if (r == 0 || r == 5) { // 5 = card not out
            success = true
            break
        }
        await wait(3000)
    }
    res.sendStatus(success ? 200 : 500)
})

app.listen(port, () => console.log(`Listening on port ${port}`))
